# Web server inside Blender, part of Colibri
# Copyright (C) 2015-2020 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import bpy
from bpy.app.handlers import persistent
from bpy.ops import op_as_string
import json
import uuid
import os.path
import queue
import threading


from wsgiref.simple_server import make_server
from ws4py.websocket import WebSocket as _WebSocket
from ws4py.server.wsgirefserver import WSGIServer, WebSocketWSGIRequestHandler
from ws4py.server.wsgiutils import WebSocketWSGIApplication



bl_info = {
    "name": "LFS Webserver",
    "author": "Les Fées Spéciales (LFS)",
    "version": (0, 0, 2),
    "blender": (2, 80, 0),
    "description": "Connect to a web service outside Blender, such as a pose library",
    "tracker_url": "https://gitlab.com/lfs.coop/blender/colibri/-/issues",
    "category": "Pipeline",
    "support": "COMMUNITY",
}

CLASSES = []


# Default is localhost only
allow_list = ['127.0.0.1']

# If False, any connection out of allow_list refused.
# If True, prompted to accept (not yet)
OPEN_SOCKET = False

port = 8137      # Server default port
port_range = 10  # Number of tried ports incrementing from 'port' variable


# Server state storage
wserver = None
wserver_thread = None
message_queue = queue.Queue()
sockets = []
callbacks = {}

is_server_running = False
message = None
previous_message = None



####### UTILS

def send_callback(instance, msg={}):
    msg['operator'] = instance.bl_idname
    bpy.ops.lfs.message_callback(callback_idx=instance.callback_idx,
                                 callback_uid=instance.callback_uid,
                                 message=json.dumps(msg))

def send_error(instance, msg={}):
    msg['errored'] = True
    send_callback(instance, msg)


def register_callback(callback):
    '''Register a callback function with an assigned uuid
       used to enable callbacks from operators to the socket'''

    k = str(uuid.uuid4())
    callbacks[k] = callback
    return k


class WebSocketApp(_WebSocket):
    '''This class handles the websockets opening,
       remote closing and message reception'''

    def opened(self):
        print("New connection opened from: %s" % self.peer_address[0])
        if self.peer_address[0] not in allow_list and OPEN_SOCKET is False:
            print("Incoming connection from %s refused" % self.peer_address[0])
            self.close(code=1008, reason="Connection not allowed")
            return

        sockets.append(self)

    def closed(self, code, reason=None):
        print('Connection from %s closed %i: %s'
                    % (self.peer_address[0], code, reason))
        sockets.remove(self)

    def received_message(self, message):
        print("Incoming message from %s" % self.peer_address[0])
        # Queing the message (full) and the socket used
        message_queue.put((message.data.decode(message.encoding), self))


def start_server(op, host, port):
    '''Start a webserver'''

    global wserver, wserver_thread
    if wserver:
        print("Server already running?")
        op.report({'WARNING'}, "Server already running?")
        return False

    source_port = port
    while not wserver or source_port + port_range <= port:
        print("Starting server on port %i" % port)

        op.report({'INFO'}, "Starting server on port %i" % port)
        try:
            wserver = make_server(host, port, server_class=WSGIServer,
                                  handler_class=WebSocketWSGIRequestHandler,
                                  app=WebSocketWSGIApplication(handler_cls=WebSocketApp)
            )
            wserver.initialize_websockets_manager()
        except:
            print("Unable to start the server on port %i" % port)
            op.report({'WARNING'}, "Unable to start the server on port %i" % port)
            port += 1
            if source_port + port_range <= port:
                print("Tried all ports without finding one available")
                op.report({'WARNING'}, "Tried all ports without finding one available")
                return

    wserver_thread = threading.Thread(target=wserver.serve_forever)
    wserver_thread.daemon = True
    wserver_thread.start()
    return True


def stop_server():
    '''Stopping the webserver, closing the sockets, ...'''

    global wserver, wserver_thread
    if not wserver:
        return False

    for socket in sockets:
        socket.close(code=1001)
    wserver.shutdown()
    wserver.server_close()
    wserver_thread.join()
    wserver_thread._stop()  # not documented but working well
    wserver = None
    print("Stopped server\n")
    return True


def process_queue_timer():
    '''Periodically process the task queue'''
    global previous_message, message
    while not message_queue.empty():
        incoming_message, socket = message_queue.get()
        try:
            callback_idx = register_callback(lambda callback_msg: socket.send(callback_msg))
            if not check_message(incoming_message=incoming_message, callback_idx=callback_idx):
                continue
            if ('event_id' in message and 'event_id' in previous_message
                    and message['event_id'] == previous_message['event_id']
                    and not message_queue.empty()):
                continue
            previous_message = message
            dispatch_message(callback_idx=callback_idx)
        except Exception as e:
            print("MESSAGE ERROR:", e)
    return 0.1


def operator_exists(idname):
    '''Check if an operator exists'''
    namespace, opname = idname.split(".")
    try:
        op = getattr(getattr(bpy.ops, namespace), opname)
        op.__repr__()
    except AttributeError:
        return False
    return True

def check_message(incoming_message, callback_idx):
    # Message should be a JSON string containing a least an operator
    # parameter to call

    print("message: %s" % incoming_message)
    print("callback idx: %s\n" % callback_idx)

    try:
        # Message SHOULD be a valid json string
        global message
        message = json.loads(incoming_message)
    except:
        print("Message is no valid json")
        bpy.ops.lfs.message_callback(callback_idx=callback_idx,
                                     callback_uid="",
                                     message="ERROR: message no json")
        return

    if 'operator' not in message:
        # msg lib should have an operator key
        print("JSON not well formed: no operator specified in json")
        bpy.ops.lfs.message_callback(callback_idx=callback_idx,
                                     callback_uid=message.get("callback_uid", ""),
                                     message="ERROR: no operator specified in json")
        return

    if not operator_exists(message['operator']):
        # The operator to call needs to be registered before
        print("Operator %s not defined" % message['operator'])
        bpy.ops.lfs.message_callback(callback_idx=callback_idx,
                                     callback_uid=message.get("callback_uid", ""),
                                     message="ERROR, Operator %s not defined" %
                                     message['operator'])
        return

    return True


def dispatch_message(callback_idx):
    # getting the function you're looking for
    global message
    operator_name = message["operator"]
    namespace = getattr(bpy.ops, operator_name.split(".")[0])
    op = getattr(namespace, operator_name.split(".")[1])

    message["callback_idx"] = callback_idx
    message = cleanup_keys(op, message)

    # all other arguments sent by the app are up to you!

    # call the operator, providing the message
    if op.poll():
        op(**message)
    else:
        bpy.ops.lfs.message_callback(callback_idx=callback_idx,
                                     callback_uid=message.get("callback_uid", ""),
                                     message="ERROR, Context incorrect for operator %s" %
                                     operator_name)


@persistent
def handler_register_timer_function(self, context):
    '''This makes sure that the timer used to process the task queue keeps running
when loading a different blend file.'''
    if is_server_running and not bpy.app.timers.is_registered(process_queue_timer):
        bpy.app.timers.register(process_queue_timer)


class LFS_OT_start_server(bpy.types.Operator):
    '''Simple operator to start the server'''
    bl_idname = "lfs.start_server"
    bl_label = "LFS: Start Server"


    def execute(self, context):
        global is_server_running
        if is_server_running:
            print("Stopping server...")
            is_server_running = False
            stop_server()
            if bpy.app.timers.is_registered(process_queue_timer):
                bpy.app.timers.unregister(process_queue_timer)
            return {'FINISHED'}
        else:
            port = context.scene.lfs_port
            host = context.scene.lfs_host
            start_server(self, host, port)
            bpy.app.timers.register(process_queue_timer)

            is_server_running = True
            return {'FINISHED'}


class LFS_OT_message_callback(bpy.types.Operator):
    '''Used to send messages back to the used socket'''

    bl_idname = "lfs.message_callback"
    bl_label = "LFS: Message Callback"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    message: bpy.props.StringProperty()

    def execute(self, context):
        self.message = json.dumps({"event": self.callback_uid, "data": self.message})
        print("Message Call Back to %s: %s" % (self.callback_idx, self.message))
        callbacks[self.callback_idx](self.message)
        del callbacks[self.callback_idx]  # Callback can only be used once
        return {'FINISHED'}


def cleanup_keys(op, msg):
    props = op.get_rna_type().properties
    clean_msg = {}
    for key in msg:
            if key in props:
                    clean_msg[key] = msg[key]
    return clean_msg


class LFS_OT_blender_ping(bpy.types.Operator):
    '''Simple demo operator that returns current opened file'''

    bl_idname = "lfs.blender_ping"
    bl_label = "LFS: Blender Ping"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()

    def execute(self, context):
        msgBack = {'filepath': bpy.data.filepath if bpy.data.filepath else 'untitled',
                             'filename': os.path.basename(bpy.data.filepath) if bpy.data.filepath else 'untitled'}
        send_callback(self, msgBack)
        return {'FINISHED'}

### UI

class LFS_PT_webserver_panel(bpy.types.Panel):
    '''Webserver Panel'''
    bl_label = "Colibri Webserver"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "LFS"


    def draw(self, context):
        layout = self.layout

        scene = context.scene
        col = layout.column(align=True)
        row = col.row()
        row.prop(scene, "lfs_host")
        row.prop(scene, "lfs_port")
        col.operator("lfs.start_server", text="Stop server" if is_server_running else "Start Server", icon='PAUSE' if is_server_running else "PLAY")


CLASSES.extend([
    # main / webserver
    LFS_OT_message_callback,
    LFS_OT_blender_ping,
    LFS_OT_start_server,
    LFS_PT_webserver_panel,
])


def register():
    bpy.types.Scene.lfs_host = bpy.props.StringProperty(name="Host", default="localhost", description="The host to connect the PoseLib to")
    bpy.types.Scene.lfs_port = bpy.props.IntProperty(name="Port", default=8137, description="The port to connect the PoseLib to")
    bpy.types.Scene.last_call = bpy.props.FloatProperty()

    for cl in CLASSES:
        bpy.utils.register_class(cl)

    bpy.app.handlers.load_post.append(handler_register_timer_function)


def unregister():
    for cl in reversed(CLASSES):
        bpy.utils.unregister_class(cl)

    del bpy.types.Scene.lfs_host
    del bpy.types.Scene.lfs_port
    del bpy.types.Scene.last_call

    if bpy.app.timers.is_registered(process_queue_timer):
        bpy.app.timers.unregister(process_queue_timer)
    bpy.app.handlers.load_post.remove(handler_register_timer_function)

if __name__ == "__main__":
    register()
