# Colibri 

Colibri is a pose library tool for Blender. It works outside of Blender with a
webserver saving/sharing the poses that you can reuse in Blender whenever you
need. 

## Dependencies

### ws4py

Websockets for Python needs to be callable from Blender. Download the source code
and install it in your python package. One way to install it is to use
Blender’s Python binary to run pip and install the package to your
Blender user directory.

The Python binary path can be found by running:

``` python
import bpy
print(bpy.app.binary_path_python)
```

in Blender.

Similarly, the scripts path can be found by running:

``` python
import bpy
import os
print(os.path.join(bpy.utils.user_resource("SCRIPTS"), "modules"))
```

You should then run the following command line:

``` bash
<path/to/blender's/python> -m pip install --target=<path/to/blender's/modules> ws4py
```

## License

Blender scripts shared by **Les Fées Spéciales** are, except where
otherwise noted, licensed under the GPLv2 license.
