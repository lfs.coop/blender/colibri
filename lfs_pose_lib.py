# Colibri, copyright (C) 2015-2020 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


bl_info = {
    "name": "LFS Pose Library",
    "author": "Les Fées Spéciales (LFS)",
    "version": (0, 0, 2),
    "blender": (2, 80, 0),
    "description": "Pose library operators, to be used with a complementary web app",
    "tracker_url": "https://gitlab.com/lfs.coop/blender/colibri/-/issues",
    "category": "Animation",
    "support": "COMMUNITY",
}


import bpy
import os
import tempfile
import base64
import json
from mathutils import Matrix, Vector
from math import inf


_initial_pose_data = {}

POSE = {'matrix_basis':    Matrix(),
        'bbone_curveinx':  0.0,
        'bbone_curveiny':  0.0,
        'bbone_curveoutx': 0.0,
        'bbone_curveouty': 0.0,
        'bbone_rollin':    0.0,
        'bbone_rollout':   0.0,
        'bbone_scaleinx':  1.0,
        'bbone_scaleiny':  1.0,
        'bbone_scaleoutx': 1.0,
        'bbone_scaleouty': 1.0,
        'bbone_easein':    0.0,
        'bbone_easeout':   0.0}

####### UTILS

def send_callback(instance, msg={}):
    msg['operator'] = instance.bl_idname
    bpy.ops.lfs.message_callback(callback_idx=instance.callback_idx,
                                 callback_uid=instance.callback_uid,
                                 message=json.dumps(msg))


def send_error(instance, msg={}):
    msg['errored'] = True
    send_callback(instance, msg)


def get_bones(selected=False):
    if selected:
        bones = bpy.context.selected_pose_bones
    else:
        bones = list(bpy.context.object.pose.bones)
    return bones


def get_pose(selected=False):
    '''This function gets the matrix (position/rotation/scale) and bbone props of
    all selected bones and returns them in a dictionary.
    '''
    bpy.ops.object.mode_set(mode='POSE')
    # TODO: get the active character/set?

    bones = get_bones(selected)
    bone_transforms = {}
    for bone in bones:
        data_dict = {}
        for prop, default in POSE.items():
            prop_value = getattr(bone, prop)
            if prop_value == default:
                continue
            if type(prop_value) == Matrix:
                prop_value = [tuple(e) for e in list(prop_value)]
            data_dict[prop] = prop_value

        bone_transforms[bone.name] = data_dict
    return bone_transforms


def get_anim(context):
    '''
    Get animation for selected keyframes
    '''
    bpy.ops.object.mode_set(mode='POSE')

    obj = context.active_object
    if (obj.animation_data is None or obj.animation_data.action is None
            or len(obj.animation_data.action.fcurves) == 0):
        self.report({'ERROR'}, 'No animation found')
        return {'CANCELLED'}
    anim_data = {}
    first_frame = inf
    for fcurve in obj.animation_data.action.fcurves:
        # Ignore unselected curves
        if (not fcurve.select
                and not any(point.select_control_point for point in fcurve.keyframe_points)):
            continue
        fcurve_data = {}
        points = []

        for keyframe_point in sorted(fcurve.keyframe_points, key=lambda k:k.co.x):
            # Ignore unselected points
            if not keyframe_point.select_control_point:
                continue
            first_frame = min(first_frame, keyframe_point.co.x)
            point_data = {}
            point_data['co'] = keyframe_point.co.to_tuple()
            point_data['period'] = keyframe_point.period
            point_data['easing'] = keyframe_point.easing
            point_data['interpolation'] = keyframe_point.interpolation
            point_data['handle_left'] = keyframe_point.handle_left.to_tuple()
            point_data['handle_left_type'] = keyframe_point.handle_left_type
            point_data['handle_right'] = keyframe_point.handle_right.to_tuple()
            point_data['handle_right_type'] = keyframe_point.handle_right_type
            point_data['amplitude'] = keyframe_point.amplitude
            point_data['back'] = keyframe_point.back
            point_data['type'] = keyframe_point.type

            points.append(point_data)

        fcurve_data['points'] = points
        fcurve_data['first_frame'] = first_frame

        anim_data[f'{fcurve.data_path}.{fcurve.array_index}'] = fcurve_data
    return anim_data


def apply_selection_set(selection_set_data):
    '''Apply the provided selection set to the current selected armature.'''
    bones = bpy.context.object.pose.bones
    for bone in bones:
        bone.bone.select = bone.name in selection_set_data


def apply_pose(poselib_pose_data,
               merge_factor=100,
               initial_merge=False,
               flip=False,
               apply_on_selected=False):
    '''
    This function applies the provided pose dict to the current
    selected armature.
    If a merge factor (float representing a percentage) is provided,
    the initial pose is mixed with the provided one.
    If flip is True, at the end of the process, the flip
    blender option is called to mirror the pose.
    '''

    global _initial_pose_data
    previous_mode = bpy.context.mode
    bpy.ops.object.mode_set(mode='POSE')
    merge_factor = float(merge_factor) / 100  # converted to [0, 1] range
    if initial_merge or _initial_pose_data == {}:
        _initial_pose_data = get_pose(apply_on_selected)

    bones = get_bones(apply_on_selected)

    target_bones = {b.name: b for b in bones}

    for bone_name, bone_data in poselib_pose_data.items():
        target_bone_name = bone_name
        # Find flipped bones if they exist
        if flip:
            target_bone_name = flip_bone_name(bone_name)

        # If the armature is in target_pose_data
        # and the bone is in armature
        if target_bone_name in target_bones:
            bone = target_bones[target_bone_name]

            # Initialize at rest values
            bone_pose_data = POSE.copy()

            bone_pose_data.update(poselib_pose_data[bone_name])

            for prop in POSE.keys():
                old_value = _initial_pose_data[target_bone_name].get(prop, POSE[prop])
                new_value = bone_pose_data[prop]
                if prop == 'matrix_basis':
                    old_value = Matrix(old_value)
                    new_value = Matrix(new_value)
                    if flip:
                        # Flip transform
                        # Kind of ripped off from Blender's pose_bone_do_paste() in
                        # /source/blender/editors/armature/pose_transform.c
                        translation, rotation, scale = new_value.decompose()
                        translation.x *= -1.0
                        translation = Matrix.Translation(translation)
                        rotation = rotation.to_euler()
                        rotation.y *= -1.0
                        rotation.z *= -1.0
                        rotation = rotation.to_matrix().to_4x4()
                        scale = Matrix.Diagonal(scale).to_4x4()
                        new_value = translation @ rotation @ scale
                    new_value = old_value.lerp(new_value, merge_factor)
                else:
                    if flip and prop in {'bbone_curveinx', 'bbone_curveoutx',
                                         'bbone_rollout', 'bone_rollout'}:
                        new_value *= -1.0

                    new_value = (new_value * merge_factor
                                 + old_value * (1.0 - merge_factor))
                setattr(bone, prop, new_value)

    bpy.ops.object.mode_set(mode=previous_mode)


def apply_anim(context, anim_data, current_frame):
    obj = context.active_object
    if obj.animation_data is None:
        obj.animation_data_create()
    obj_anim_data = obj.animation_data
    if obj.animation_data.action is None:
        action = bpy.data.actions.new(obj.name + "Action")
        obj.animation_data.action = action
    obj_fcurves = obj.animation_data.action.fcurves

    first_frame = min(point['first_frame'] for point in anim_data.values())

    for fcurve, fcurve_data in anim_data.items():
        data_path = ".".join(fcurve.split(".")[:-1])
        array_index = int(fcurve.split(".")[-1])
        obj_fcurve = obj_fcurves.find(data_path, index=array_index)
        if obj_fcurve is None:
            obj_fcurve = obj_fcurves.new(data_path, index=array_index)

        for keyframe_point in fcurve_data['points']:
            co = Vector(keyframe_point['co'])
            if current_frame:
                co.x += context.scene.frame_current_final - first_frame

            point = obj_fcurve.keyframe_points.insert(*co, options={'REPLACE', 'FAST', 'NEEDED'}, keyframe_type=keyframe_point['type'])

            point.type = keyframe_point['type']
            point.period = keyframe_point['period']
            point.easing = keyframe_point['easing']
            point.interpolation = keyframe_point['interpolation']
            point.handle_left = keyframe_point['handle_left']
            point.handle_left_type = keyframe_point['handle_left_type']
            point.handle_right = keyframe_point['handle_right']
            point.handle_right_type = keyframe_point['handle_right_type']
            point.amplitude = keyframe_point['amplitude']
            point.back = keyframe_point['back']

        obj_fcurve.update()

    return {'FINISHED'}

def flip_bone_name(from_name):
    """From https://pastebin.com/PnMgZNNv
    Based on BLI_string_flip_side_name in
    source/blender/blenlib/intern/string_utils.c
    """

    l = len(from_name)  # Number of characters from left to right, that we still care about. At first we care about all of them.

    # Handling .### cases
    if("." in from_name):
        # Make sure there are only digits after the last period
        before_last_period = "".join(from_name.split(".")[:-1])
        after_last_period = from_name.split(".")[-1]
        all_digits = True
        for c in after_last_period:
            if( c not in "0123456789" ):
                all_digits = False
                break
        # If that is so, then we don't care about the characters after this last period.
        if(all_digits):
            l = len(before_last_period)

    # Case: Suffix or prefix R r L l separated by . - _
    name = from_name[:l]
    new_name = name
    separators = ".-_"
    for s in separators:
        # Suffixes
        if(s+"L" == name[-2:]):
            new_name = name[:-1] + 'R'
            break
        if(s+"R" == name[-2:]):
            new_name = name[:-1] + 'L'
            break

        if(s+"l" == name[-2:]):
            new_name = name[:-1] + 'r'
            break
        if(s+"r" == name[-2:]):
            new_name = name[:-1] + 'l'
            break

        # Prefixes
        if("L"+s == name[:2]):
            new_name = "R" + name[1:]
            break
        if("R"+s == name[:2]):
            new_name = "L" + name[1:]
            break

        if("l"+s == name[:2]):
            new_name = "r" + name[1:]
            break
        if("r"+s == name[:2]):
            new_name = "l" + name[1:]
            break

    if(new_name != name):
        return new_name + from_name[l:]

    # case: "left" or "right" with any case found anywhere in the string.

    left = ['left', 'Left', 'LEFT']
    right = ['right', 'Right', 'RIGHT']

    lists = [left, right, left] # To get the opposite side, we just get lists[i-1]. No duplicate code, yay!

    # Trying to find any left/right string.
    for list_idx in range(1, 3):
        for side_idx, side in enumerate(lists[list_idx]):
            if(side in name):
                # If it occurs more than once, only replace the last occurrence.
                before_last_side = "".join(name.split(side)[:-1])
                after_last_side = name.split(side)[-1]
                opp_side = lists[list_idx-1][side_idx]
                return before_last_side + opp_side + after_last_side + from_name[l:]
    # If nothing was found, return the original string.
    return from_name


def select_bones(json_data):
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    json_data = json.loads(json_data.decode())
    bones = []
    if bones == []:
        for rig in [r for r in bpy.data.objects if r.type == 'ARMATURE']:
            for bone in rig.pose.bones:
                bones.append(bone)
    for bone in bones:
        if json_data.get(bone.name):
            bone.bone.select = True


class LFS_OT_colibri_apply_pose(bpy.types.Operator):
    '''Get a pose as a base64-encoded JSON and apply it'''
    bl_idname = "lfs.colibri_apply_pose"
    bl_label = "LFS: Apply pose"

    # Props common to modes
    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    pose_b64: bpy.props.StringProperty()

    pose_type: bpy.props.EnumProperty(items=(('POSE', "Pose", "Apply pose"),
                                             ('SELECTION_SET', "Selection Set", "Select bones only"),
                                             ('ANIMATION', "Animation", "Apply animation")),
                                      default='POSE')

    # Pose mode props
    flip: bpy.props.BoolProperty(default=False)
    apply_on_selected: bpy.props.BoolProperty(default=False)
    # For merging poses
    merge_factor: bpy.props.IntProperty(default=100)
    initial_merge: bpy.props.BoolProperty(default=False)

    # Anim mode prop
    current_frame: bpy.props.BoolProperty(default=False)

    @classmethod
    def poll(self, context):
        return context.active_object is not None and context.active_object.type == "ARMATURE"

    def execute(self, context):
        data = json.loads(base64.b64decode(self.pose_b64))
        if self.pose_type == 'POSE':
            apply_pose(poselib_pose_data=data,
                       initial_merge=self.initial_merge,
                       merge_factor=self.merge_factor,
                       flip=self.flip,
                       apply_on_selected=self.apply_on_selected)
        elif self.pose_type == 'SELECTION_SET':
            apply_selection_set(selection_set_data=data)
        elif self.pose_type == 'ANIMATION':
            apply_anim(context, anim_data=data, current_frame=self.current_frame)

        send_callback(self)
        return {'FINISHED'}


class LFS_OT_colibri_make_snapshot(bpy.types.Operator):
    '''Make a snapshot (openGlRender) and send it to the server'''
    bl_idname = "lfs.colibri_make_snapshot"
    bl_label = "LFS: Make a snapshot"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    post_url: bpy.props.StringProperty()

    def get_context_dict(self, context):
        # Find 3D View
        area_view_3d = None
        try:
            area_index = context.window_manager.colibri_active_view
            area_view_3d = context.screen.areas[area_index]
            if area_view_3d.type != "VIEW_3D":
                # Active area is not a 3D view. Layout must have changed.
                # Whatever, use first 3D view in layout.
                area_view_3d = None
                i = 0
                for area in context.screen.areas:
                    if area.type == 'VIEW_3D':
                        area_view_3d = area
                        break
        except:
            pass

        context = context.copy()
        context['area'] = area_view_3d
        context['region'] = area_view_3d.regions[4]
        context['region_data'] = area_view_3d.spaces[0].region_3d
        return context

    def execute(self, context):
        # From https://stackoverflow.com/a/31174427
        # This gets and sets attributes recursively
        import functools
        def rsetattr(obj, attr, val):
            pre, _, post = attr.rpartition('.')
            return setattr(rgetattr(obj, pre) if pre else obj, post, val)

        def rgetattr(obj, attr, *args):
            def _getattr(obj, attr):
                return getattr(obj, attr, *args)
            return functools.reduce(_getattr, [obj] + attr.split('.'))

        # Create temp file
        with tempfile.TemporaryDirectory() as d:
            path = os.path.join(d, "image.jpg")

            # Set render settings
            settings = {'filepath': path,
                        'resolution_x': 600,
                        'resolution_y': 600,
                        'resolution_percentage': 100,
                        'image_settings.file_format': 'JPEG',
                        'image_settings.color_mode': 'RGB'}
            settings_orig = {}

            # Save the original settings and apply the thumbnail ones
            for k, v in settings.items():
                settings_orig[k] = rgetattr(context.scene.render, k)
            for k, v in settings.items():
                rsetattr(context.scene.render, k, v)

            # Render openGL and write the render
            render_context = self.get_context_dict(context)
            if render_context['area'] is None:
                self.report({'ERROR'}, "No 3D View found")
                return {'CANCELLED'}

            bpy.ops.render.opengl(render_context, write_still=True)

            # Restore previous settings
            for k, v in settings_orig.items():
                rsetattr(context.scene.render, k, v)

            # Open the rendered image and encode it as base64
            with open(path, "rb") as image_file:
                encoded_image = base64.b64encode(image_file.read())

            # Callback to warn the image is uploaded
            send_callback(self, {"content": "data:image/jpeg;base64,%s" % encoded_image.decode()})
        return {'FINISHED'}


class LFS_OT_colibri_get_pose(bpy.types.Operator):
    '''Get a pose a a base64 encoded json and apply it '''

    bl_idname = "lfs.colibri_get_pose"
    bl_label = "LFS: Get pose"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    pose_type: bpy.props.EnumProperty(items=(('POSE', "Pose", "Apply pose"),
                                             ('SELECTION_SET', "Selection Set", "Select bones only"),
                                             ('ANIMATION', "Animation", "Apply animation")),
                                      default='POSE')

    def execute(self, context):
        if self.pose_type in {'POSE', 'SELECTION_SET'}:
            pose = get_pose(selected=True)
        elif self.pose_type == 'ANIMATION':
            pose = get_anim(context)
        p = json.dumps(pose, indent=None, separators=(',', ':'))
        send_callback(self, {'pose_b64': base64.b64encode(p.encode('ascii')).decode()})
        return {'FINISHED'}


def get_area_index(context, area):
    '''Get specific area's index in the current screen's areas list'''
    for i, ar in enumerate(context.screen.areas):
        if area == ar:
            return i


class LFS_OT_colibri_activate_view(bpy.types.Operator):
    '''Choose a View3D as active. This View will then be used for
    the OpenGL screenshot
    '''
    bl_idname = "lfs.colibri_activate_view"
    bl_label = "Activate 3D View for thumbnail"

    def execute(self, context):
        area_index = get_area_index(context, context.area)
        context.window_manager.colibri_active_view = area_index

        # Force refresh view
        for area in context.screen.areas:
            area.tag_redraw()
        return {'FINISHED'}


class LFS_PT_Colibri_Active_3D_View(bpy.types.Panel):
    '''Panel to set active area for Colibri'''
    bl_label = "Active 3D View"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "LFS"
    bl_parent_id = 'LFS_PT_webserver_panel'

    def draw(self, context):
        layout = self.layout

        area_index = get_area_index(context, context.area)
        is_active_area = area_index == context.window_manager.colibri_active_view

        col = layout.column(align=True)
        row = col.row()
        row.active = not is_active_area
        row.operator("lfs.colibri_activate_view")

        if is_active_area:
            col.label(text="This is the active 3D view", icon="INFO")


CLASSES = [
    LFS_OT_colibri_apply_pose,
    LFS_OT_colibri_make_snapshot,
    LFS_OT_colibri_get_pose,
    LFS_OT_colibri_activate_view,
    LFS_PT_Colibri_Active_3D_View
]

def register():
    # From https://blenderartists.org/t/check-if-add-on-is-enabled-using-python/522226/2
    import addon_utils
    mod_name = "lfs_blenderwebserver"
    is_enabled, is_loaded = addon_utils.check(mod_name)
    if not is_enabled:
        # Activate web server
        # TODO error handling lol
        addon_utils.enable(mod_name, default_set=True)

    for cl in CLASSES:
        bpy.utils.register_class(cl)
    bpy.types.WindowManager.colibri_active_view = bpy.props.IntProperty(default=0, min=0, name="Active View")

def unregister():
    for cl in reversed(CLASSES):
        bpy.utils.unregister_class(cl)
    del bpy.types.WindowManager.colibri_active_view

if __name__ == "__main__":
    register()
